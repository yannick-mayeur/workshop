The following requirements must be met:

1. You have an account on <https://gitlab.com>
1. You have created an SSH key and have uploaded it to GitLab. Follow the steps in <https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair>.

Now that you have a GitLab.com account and you have uploaded your SSH key,
we can create a new project.
