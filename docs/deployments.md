In the last part of the workshop, we'll take a look into Continuous Deployment.
Now that our app is properly tested, we can deploy it and see it live.

!!! todo "Objectives"
    1. Spin up a virtual machine locally and install the GitLab Runner.
    1. Register the Runner with our project.
    1. Create a Dockerfile for our app.
    1. Build the Docker image and upload it to GitLab's Container Registry.
    1. Add a deployment job to deploy our application every time a commit
       is pushed on master.
    1. Learn about environments and deployments.

## The production server

We'll use a virtualbox VM locally to serve as our production server.

### Import VM

Open VirtualBox and import the machine.

### Use a VM from scratch

The easiest way is to use Vagrant:

1. Install [Vagrant](https://www.vagrantup.com/docs/index.html) on your machine.
1. Create a file named `Vagrantfile` on your machine with the following contents:

     ```
     # -*- mode: ruby -*-
     # vi: set ft=ruby :

     Vagrant.configure("2") do |config|
       config.vm.box = "ubuntu/bionic64"

       config.vm.network "private_network", ip: "10.0.0.10"

       config.vm.network "public_network"

       config.vm.provider "virtualbox" do |vb|
         vb.gui = false
         vb.memory = "1024"
       end

       config.vm.provision "shell", inline: <<-SHELL
         apt-get update
         apt-get -qq upgrade
       SHELL
     end
     ```

1. With your terminal, navigate to where the file was saved and enter the command:

     ```sh
     vagrant up
     ```

    Wait a few minutes and an Ubuntu machine will be downloaded and provisioned.

1. Once it finishes, run:

     ```sh
     vagrant ssh
     ```

You can then proceed to install and configure Docker and GitLab Runner.

#### Install Docker

While inside the vagrant VM, install Docker:

```sh
sudo apt update

sudo apt -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt update

sudo apt install -y docker-ce docker-ce-cli containerd.io
```

#### Install the GitLab Runner

While inside the vagrant VM, install GitLab Runner:

```sh
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

sudo apt install gitlab-runner
```

### After import

You'll need to find the VM's IP (VirtualBox should have already assigned it). On
your local machine, open a terminal and run:

```sh
# On macOS
ifconfig -a
```

or:

```sh
# On Linux
ip addr
```

You can then connect to the virtual machine with:

```sh
ssh vagrant@<ip>
```

The password is `vagrant`.

### Register the GitLab Runner

In order to get the Runner to talk to GitLab, you need to register it:

1. SSH into the VM (see the previous section).
1. Obtain a token under your project's **Settings > CI/CD > Runners**.
   Make sure you copy its value.
1. Back on the VM, register the Runner with:

    ```
    sudo gitlab-runner register
    ```

    Add the following when asked:

    - Coordinator: `https://gitlab.com`.
    - Token: the one you obtained in the first step.
    - Description: `Deploy Runner`
    - Tags: `deploy`
    - Executor: `shell`

1. You can verify that all went well with:

    ```sh
    sudo gitlab-runner verify
    ```

After the registration, you should see the Runner in the same page when you
obtained the token.

Now that the Runner is set up, you can use it for your jobs by specifying it
with (don't do it yet):

```yaml
tags:
  - deploy
```

`tags` is used to select specific Runners from the list of all Runners that are
allowed to run this project.

!!! note
    Read more about `tags` https://docs.gitlab.com/ee/ci/yaml/#tags.

## Create the app Docker image

We now need to create the Docker image that will have our app. We will then
use that image to deploy the application to our server.

Create a file at the root of your repository called `Dockerfile` with the following
contents:

```
FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

ARG PORT
ENV PORT ${PORT:-5000}
EXPOSE $PORT

CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
#CMD [ "sh", "-c", "java -Dserver.port=$PORT -jar /usr/src/app/spring-petclinic-2.1.0.BUILD-SNAPSHOT.jar" ]
```

!!! todo "Advanced"
    How can we use the existing jar file from the `build` job without having to
    rebuild the app each time? Hint: https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html#downloading-the-latest-artifacts.

## Push to GitLab Container Registry

Build and upload your project's Docker image by adding the following to `.gitlab-ci.yml`:

```yaml
build docker:
  stage: docker-build
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    PORT: 5000
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker build --build-arg PORT=$PORT -t $IMAGE_TAG:latest -t $IMAGE_TAG:$CI_COMMIT_SHA .
    - docker push $IMAGE_TAG:latest
    - docker push $IMAGE_TAG:$CI_COMMIT_SHA
  only:
    - master
```

Wait a few minutes and when the pipeline passes, navigate to your project's
**Registry** to see the uploaded Docker image.

!!! tip
    Read more about using GitLab's Container Registry to build Docker images https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-the-gitlab-container-registry.
    The variables starting with `CI_` are predefined by GitLab.

## Ready to deploy

These are the final steps to deploy our app to our server.

!!! note
    The way this deployment is implemented is for demonstration purposes.
    In a production environment, you would deploy your Docker application
    with Kubernetes or some other container orchestrator.

### Give permissions to the Runner

1. SSH into the VM
1. Get root access:

    ```
    sudo -i
    ```

1. Give permissions to the Runner:

    ```
    echo 'gitlab-runner ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/runner
    ```

1. Exit the VM with `exit`.

### Create the system service for our app

There's also a final step for the deployment to work, and that is to create a
systemd file in the VM (server) that will pull the app's Docker image and run it.

1. Enter into the VM, and as root, create `/etc/systemd/system/spring.service`
   with the following contents:

    ```ini
    [Unit]
    Description=Spring Container
    After=docker.service
    Requires=docker.service

    [Service]
    TimeoutStartSec=0
    Restart=always
    ExecStartPre=/usr/bin/docker stop egistry.gitlab.com/<username>/spring-petclinic-two/master:latest
    ExecStartPre=/usr/bin/docker rm registry.gitlab.com/<username>/spring-petclinic-two/master:latest
    ExecStartPre=/usr/bin/docker pull registry.gitlab.com/<username>/spring-petclinic-two/master:latest
    ExecStart=/usr/bin/docker run --rm --name %n registry.gitlab.com/<username>/spring-petclinic-two/master:latest

    [Install]
    WantedBy=multi-user.target
    ```

1. Replace `username` with your GitLab username.
1. Once you save the file, reload systemd:

    ```sh
    sudo systemctl daemon-reload
    ```

### Create the deployment job

Now that the Docker image is built each time a commit is pushed on master, we
can create a deploy job and use that image to deploy it to our server.

```yaml
deploy:
  stage: deploy
  tags:
    - deploy
  script:
    - sudo systemctl restart spring.service
  only:
    - master
```

Every time you merge something into master, the `deploy` job will run and deploy
the app to the server.

## Enabling the Container scanning job

Add the Container Scanning job.

!!! todo "Objectives"
    1. Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/12.
    1. Once the job is set up, check the Security dashboard and create an issue from a vulnerability.

