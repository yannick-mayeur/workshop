To learn about GitLab CI, we'll create a project, add some files into it, and then
add some jobs in `.gitlab-ci.yml`.

## Create a new project

First, create a new project:

1. In your dashboard, click the green **New project** button or
   use the plus icon in the navigation bar. This opens the New project page.
1. On the New project page, choose to create a blank project, give it a name
   (e.g., `gitlab-ci`) and check to initialize the repository with a README.
1. Click **Create project**.

!!! tip
    You can create a new project by simply pushing a repository from your
    command line. [Learn more](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#push-to-create-a-new-project).

## Clone the repository locally

Now that a new project is created, we'll clone it locally:

```sh
git clone git@gitlab.com:<username>/gitlab-ci.git
```

Where `<username>` is your GitLab username.

!!! tip
    Use the "Clone with SSH" option so that you don't have to type your
    password every time you push.

## The first pipeline

Now, it's time to create a `.gitlab-ci.yml` file that will utilize GitLab CI.
Add the following contents:

```yaml
I love cats:
  script:
    - sleep 10
    - cat README.md
```

Once done, save the file, commit and push to GitLab.

```sh
git add .gitlab-ci.yml
git commit -m "Add .gitlab-ci.yml"
git push origin master
```

Navigate to your project's **Pipelines** page (left sidebar under CI / CD section).
Your first pipeline should be up and running with the help of the shared
Runners that GitLab.com provides for free.

This is a pipeline that consists of only one job. Let's add another one:

```yaml
I love cats:
  script:
    - sleep 10
    - cat README.md

hello world:
  script
    - sleep 20
    - echo "Hello world!"
```

Save, commit and push once more. Navigate to the pipelines page to see what
happened.

!!! todo
    Where is the error? Find the failure, fix it, and push again.
    Hint: compare the two `script`s. What's missing from the second job?

## In parallel or in serial?

The previous two jobs, where running in parallel, both under the `test` stage.
This is the default stage if nothing is defined.

`stages` is used to define stages that can be used by jobs and is defined globally.

The specification of stages allows for having flexible multi stage pipelines.
The ordering of elements in stages defines the ordering of jobs’ execution:

- Jobs of the same stage are run in parallel.
- Jobs of the next stage are run after the jobs from the previous stage complete successfully.

Let's add a third job with a `build` stage and define all of them at the
top. This is what the end result will look like:

```yaml
stages:
  - build
  - test

multidir:
  stage: build
  script:
    - mkdir hello/world/

I love cats:
  stage: test
  script:
    - sleep 10
    - cat README.md

hello world:
  stage: test
  script:
    - sleep 20
    - echo "Hello world!"
```

!!! question "Questions"
    1. What should happen?
    1. What actually happened?
    1. How to make the jobs of the `test` stage run successfully?

    Hint: the `mkdir` command needs the `-p` flag in order to create the
    directories.

## Using a different Docker image

If you don't define a Docker image, the default one is used.
The [shared Runners](https://docs.gitlab.com/ee/user/gitlab_com/#shared-runners)
that run on GitLab.com use `ruby:2.5` by default.

Depending on your project, you can use any Docker image that's hosted
on Docker Hub or any other container registry.

!!! tip
    Read more in the [images documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html).

### Per job

Let's edit our `.gitlab-ci.yml` and add a different image in the `hello world`
job:

```yaml
hello world:
  image: alpine:latest
  stage: test
  script:
    - sleep 20
    - echo "Hello world!"
```

!!! question
    What do you think will happen? A smaller image should result into quicker
    builds, so the two jobs should finish around the same time.

### Per pipeline

To make all the jobs use the same image, add it at the top of the file outside
of a job's definition:

```yaml
image: alpine

stages:
  - build
  - test

I only make dirs:
  stage: build
  script:
    - mkdir -p hello/world/

I love cats:
  stage: test
  script:
    - sleep 10
    - cat README.md

hello world:
  stage: test
  script:
    - sleep 20
    - echo "Hello world!"
```

## Artifacts

`artifacts` is used to specify a list of files and directories which should be
attached to the job after success. They will be sent to GitLab after the job
finishes successfully and will be available for download in the GitLab UI.

Let's add a simple `index.html` file in the repository:

```html
<html>
	<body>
		<h3>
			<center>GitLab @ Polytech</center>
		</h3>
	</body>
</html>
```

Create a new job that will upload it as an artifact:

```yaml
html upload:
  stage: test
  script:
    - echo "Hello world!"
  artifacts:
    paths:
      - index.html
    expire_in: 1d
```

Navigate to the job details log and browse the artifacts.

!!! question
    What will happen if you try to upload an artifact outside of the working
    directory? Try to include `- /etc/hosts` under the artifacts paths.

!!! todo
		Create a new job named `test upload` that will:
    1. Run after `html upload` in a new stage called `upload`.
    1. Contain a file with contents `Polytech Montpellier` as an artifact.

!!! tip
    Read the [artifacts documentation](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html).

### Pass artifacts between jobs

By default, all the artifacts are passed from a stage to the next one. You can optimize by choosing the artifacts pass between jobs using [`dependencies`](https://docs.gitlab.com/ee/ci/yaml/#dependencies):

```yaml
image: alpine

stages:
  - build
  - test
  - upload

I only make dirs:
  stage: build
  script:
    - mkdir -p hello/world/

I love cats:
  stage: test
  script:
    - sleep 10
    - cat README.md

hello world:
  stage: test
  script:
    - sleep 20
    - echo "Hello world!"

html upload:
  stage: test
  script:
    - echo "Hello world!"
  artifacts:
    paths:
      - index.html
    expire_in: 1d

text upload:
  stage: upload
  dependencies:
    - html upload
  script:
    - echo "Montpellier Polytech" > mpl.txt
  artifacts:
    paths:
      - index.html
      - mpl.txt
    expire_in: 1d
```

!!! tip
    Note that artifacts from all previous stages are passed by default. Defining an empty array of dependencies will skip downloading any artifacts for that job.



## Variables

Variables are useful for customizing your jobs when you don't want to use
hardcoded values. That can be very powerful as it can be used for scripting
without the need to know the value itself.

GitLab reads `.gitlab-ci.yml`, sends output to Runner, Runner runs the script
commands in its environment, under which the variables are exposed.

### Pre-defined environment variables

GitLab provides a pre-defined set of environment variables that can be used in
the jobs.

For example, two jobs under the same pipeline can share the same `CI_PIPELINE_ID`,
but each one has its own `CI_JOB_ID` variable.

Add the following job and see what happens:

```yaml
test variables:
  stage: test
  script:
    - echo "This project is called $CI_PROJECT_NAME and is under the group $CI_PROJECT_NAMESPACE."
    - echo
    - echo "The user that started this job is $GITLAB_USER_NAME."
```

!!! todo
    Find the right environment variable and make the previous job to output its name.

!!! tip
    See the [documentation](https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables)
    for a list of all environment variables.

### User set variables

Apart from the predefined variables, you can set your own variables in `.gitlab-ci.yml`
by using the `variables` definition. Let's say that we wanted to use the same
string in multiple jobs, so in order to avoid defining it in multiple places,
we do it one time:

```yaml
image: alpine

stages:
  - build
  - test
  - upload

variables:
  WORKSHOP_URL: 'https://jug-montpellier.gitlab.io/polytech/workshop/'

I only make dirs:
  stage: build
  script:
    - mkdir -p hello/world/

I love cats:
  stage: test
  script:
    - sleep 10
    - cat README.md

hello world:
  stage: test
  script:
    - sleep 20
    - echo "Hello world!"

html upload:
  stage: test
  script:
    - echo "Hello world!"
    - echo "The workshop's URL is $WORKSHOP_URL"
  artifacts:
    paths:
      - index.html
    expire_in: 1d

text upload:
  stage: upload
  dependencies:
    - html upload
  script:
    - echo "Montpellier Polytech" > mpl.txt
    - echo "The workshop's URL is $WORKSHOP_URL" >> mpl.txt
  artifacts:
    paths:
      - index.html
      - mpl.txt
    expire_in: 1d

test variables:
  stage: test
  script:
    - echo "This project is called $CI_PROJECT_NAME and is under the group $CI_PROJECT_NAMESPACE."
    - echo
    - echo "The user that started this job is $GITLAB_USER_NAME."
    - echo
    - echo $TOKEN > secret_token.txt
```

!!! todo
    Define the `WORKSHOP_URL` variable in the context of the `html upload`
    job and give it a different value. What would happen?

### Secret variables

In case your job should contain a token or something similar that must not
be exposed in `.gitlab-ci.yml` (thus committed in your Git history), you can
assign it to a variable in your project's settings.

1. Go to your project's **Settings > CI/CD Environment variables** and expand
   the section.
1. For the variable key add `TOKEN` and as the value add `s3cr3t_42`.
1. Click on **Save variables**.

Now add it to the `test variables` job:

```yaml
test variables:
  stage: test
  script:
    - echo "This project is called $CI_PROJECT_NAME and is under the group $CI_PROJECT_NAMESPACE."
    - echo
    - echo "The user that started this job is $GITLAB_USER_NAME."
    - echo
    - echo $TOKEN > secret_token.txt
  artifacts:
    paths:
      - secret_token.txt
```

!!! warning
    Be aware that variables are not masked, and their values can be shown in the
    job logs if explicitly asked to do so. If your project is public or internal,
    you can [set the pipelines private](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#visibility-of-pipelines)
    from your project’s Pipelines settings. Follow the discussion in issue
    [#13784](https://gitlab.com/gitlab-org/gitlab-ce/issues/13784) for masking the variables.

!!! tip
    Another case for defining a variable in your project's settings is that you don't
    have to change it in your `.gitlab-ci.yml` file if you need to.

## Working with the Web IDE

The web IDE is another option to edit files via GitLab without touching the
command line on your local machine.

In your project's dashboard, find the **Web IDE** button and click it.

On the left sidebar, there are three icons which denote the mode you're
currently on:

- Edit mode
- Review mode
- Commit mode

While on Edit mode, you can see the files that are present in your repository. Let's
open `index.html` and add another line:

```html
<html>
	<body>
		<h3>
			<center>GitLab @ Polytech</center>
      </br>
			<center>2019</center>
		</h3>
	</body>
</html>
```

!!! tip
    You can open as many files as you want to edit at the same time.

1. Now, let's choose the Review mode. You can see the diff of the changes you made.
   The **Commit...** button takes you straight to the Commit mode. Let's get to it.

1. You can see the unstaged/staged changes as well as the diff. Write a commit
   message and leave the option to commit to master branch. Click on **Stage & Commit**.

1. In the right sidebar, click on the rocket icon. You can watch the pipeline running
   for the changes you made. Wait for the `html upload` to succeed and click
   on its job to check the artifacts. Is the change you made present?

1. Finally, go back to your project by clicking the project's name on the upper
   left corner of the web IDE.

!!! todo "Optional"
    Make a new change via the web IDE, but this time commit by creating a branch.
