In this part of the workshop, we'll work together on a project. We'll assign issues,
submit merge requests, and work together on adding code quality and security
metrics.

Every merge request consists of a branch which contains changes and is based
off the master branch (which is set as the default branch).
Every issue must have one or more associated merge requests.

One way to to create a merge request is by manually creating the branch
on your command line. Once you add and commit your changes, you can push the
new branch to GitLab. A URL will appear which will guide you to create the merge request.

Another way is to create the branch and merge request via the associated issue.
The merge request will not contain any changes, so you'll have to fetch the
new branch locally and work on that. Then, you make change, you commit and push.

Once the merge request is ready, wait for a review.

!!! tip
    You can mark a merge request as Work In Progress (WIP) (find the button
    right below the merge request title). That way, your
    colleagues will know that you're still working on it and is not ready to
    merge.

!!! tip
    Depending on your workflow, `master` may not be the desired default branch.
    You can change the default branch in the project's settings.

## Fork and set up the remote

Go to https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two and fork
the project to your own namespace.

Once done, clone your fork locally:

1. Go your project's dashboard and find the clone button.
1. Copy the SSH command and clone the repo:

   ```sh
   git clone git@gitlab.com:<username>/spring-petclinic-two.git
   ```

1. Add the remote so that you can pull any changes:

   ```sh
   git remote add upstream https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two.git
   ```

Now that you have the repo cloned locally, we can start collaborating.

## The Spring app example

The Spring app example is taken from https://github.com/spring-projects/spring-petclinic.

As you can see, this is a very simple Spring app with two jobs:

```yaml
image: java:8

maven test:
  stage: test
  script:
    - ./mvnw test
  only:
    - branches

maven build:
  stage: build
  script:
    - ./mvnw clean install -DskipTests
  artifacts:
    paths:
      - "target/spring-petclinic-*.jar"
    expire_in: 1d
```

## Issues, milestones and issue boards

Check the issue tracker of the existing issues that we will work on
https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues.

There are some [existing labels](https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/labels) assigned to each issue.

!!! todo
    Do we need more labels? Find similar issues and decide if that makes sense.

You can also visualize all issues via the [issue board](https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/boards/1009764).

There is also a milestone that should be assigned to all issues.

!!! todo
    How to assign all issues to the current milestone.

## Merge requests and code review

!!! info "Objectives"
    Learn about:

    - Creating a merge request
    - Issue closing pattern
    - Comments, discussion, approvals

### Code Quality

Add a code quality job that will determine if a new merge request impacts the code quality.

!!! todo "Objective"
    Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/3.

!!! tip
		Read mode in the [Code Quality documentation](https://docs.gitlab.com/ee/ci/examples/code_quality.html).

### Add JUnit tests

With JUnit, GitLab can directly show any failing tests to the merge request,
without the need to go see the job log.

!!! todo "Objectives"
    1. Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/4.
    1. Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/5.

### Security testing

Add the SAST and dependency scanning jobs.

!!! todo "Objectives"
    1. Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/11.
    1. Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/13.
    1. Once the job is set up, check the Security dashboard and create an issue from a vulnerability.

### Use GitLab Pages to upload documentation

Create and upload the docs to GitLab Pages

!!! todo "Objectives"
    1. Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/9.
    1. Work on https://gitlab.com/jug-montpellier/polytech/spring-petclinic-two/issues/10.
    1. Can we combine the two jobs above into one? Create an issue first.
    1. How can we make it so that the `pages` job runs only on the `master` branch
       of the upstream project?

!!! tip
    For the last objective, search for the `only` definition in GitLab docs.
