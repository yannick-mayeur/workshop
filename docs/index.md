# GitLab workshop

In this workshop we'll learn how to work with GitLab and GitLab CI/CD.

It consists of two parts:

1. Create your own project and a simple `.gitlab-ci.yml` file.
1. Fork and collaborate on an existing project.
